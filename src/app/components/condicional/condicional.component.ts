import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-condicional',
  templateUrl: './condicional.component.html',
  styleUrls: ['./condicional.component.css']
})
export class CondicionalComponent {

  mostrar = true;
  frase: any = {
    Nombre: "Luis Fernando Camacho Velasco", 
    Direccion: "Avenida Blango Galindo Km 5 1/2"
  };
}
